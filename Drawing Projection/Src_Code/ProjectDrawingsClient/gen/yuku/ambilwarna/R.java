/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */
package yuku.ambilwarna;

public final class R {
	public static final class attr {
		public static final int supportsAlpha = 0x7f010000;
	}
	public static final class dimen {
		public static final int ambilwarna_hsvHeight = 0x7f050000;
		public static final int ambilwarna_hsvWidth = 0x7f050001;
		public static final int ambilwarna_hueWidth = 0x7f050002;
		public static final int ambilwarna_spacer = 0x7f050003;
	}
	public static final class drawable {
		public static final int ambilwarna_alphacheckered = 0x7f020057;
		public static final int ambilwarna_alphacheckered_tiled = 0x7f020058;
		public static final int ambilwarna_arrow_down = 0x7f020059;
		public static final int ambilwarna_arrow_right = 0x7f02005a;
		public static final int ambilwarna_cursor = 0x7f02005b;
		public static final int ambilwarna_hue = 0x7f02005c;
		public static final int ambilwarna_target = 0x7f02005d;
	}
	public static final class id {
		public static final int ambilwarna_alphaCheckered = 0x7f06005d;
		public static final int ambilwarna_alphaCursor = 0x7f060060;
		public static final int ambilwarna_cursor = 0x7f06005f;
		public static final int ambilwarna_dialogView = 0x7f060059;
		public static final int ambilwarna_newColor = 0x7f060064;
		public static final int ambilwarna_oldColor = 0x7f060063;
		public static final int ambilwarna_overlay = 0x7f06005e;
		public static final int ambilwarna_pref_widget_box = 0x7f060065;
		public static final int ambilwarna_state = 0x7f060062;
		public static final int ambilwarna_target = 0x7f060061;
		public static final int ambilwarna_viewContainer = 0x7f06005a;
		public static final int ambilwarna_viewHue = 0x7f06005c;
		public static final int ambilwarna_viewSatBri = 0x7f06005b;
	}
	public static final class layout {
		public static final int ambilwarna_dialog = 0x7f03001f;
		public static final int ambilwarna_pref_widget = 0x7f030020;
	}
	public static final class styleable {
		public static final int[] AmbilWarnaPreference = { 0x7f010000 };
		public static final int AmbilWarnaPreference_supportsAlpha = 0;
	}
}
