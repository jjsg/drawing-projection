package client.projectDrawings.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import client.projectDrawings.draw.BluetoothActivity;
import client.projectDrawings.draw.DrawActivity;
import client.projectDrawings.draw.MainActivity;
import client.projectDrawings.draw.R;
import client.projectDrawings.extraStrings.Controls;

public class DrawingView extends View {

	public int wid, ht;
	// drawing path
	private Path drawPath;

	// drawing and canvas paint
	private Paint drawPaint, canvasPaint;

	// initial color
	public int paintColor = 0xFF660000;

	// canvas
	private Canvas drawCanvas;

	// canvas bitmap
	private Bitmap canvasBitmap;

	public Bitmap textBitmap = null;

	private float oldX = 0.0f, oldY = 0.0f;

	private float brushSize;

	// reverse switch from eraser(save old brush size)
	private float lastBrushSize;

	private boolean erase = false;

	public boolean isErase() {
		return erase;
	}

	// textbox
	public boolean forTextBox = false, confirm = false;
	public float x, y;
	public EditText edit;

	public DrawingView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setupDrawing(context);
	}

	private void setupDrawing(Context context) {

		brushSize = getResources().getInteger(R.integer.medium_size);
		lastBrushSize = brushSize;

		// get drawing area setup for interaction
		drawPath = new Path();
		drawPaint = new Paint();

		drawPaint.setColor(paintColor);

		drawPaint.setAntiAlias(true);
		drawPaint.setStrokeWidth(brushSize);// medium
		drawPaint.setStyle(Paint.Style.STROKE);
		drawPaint.setStrokeJoin(Paint.Join.ROUND);
		drawPaint.setStrokeCap(Paint.Cap.ROUND);

		canvasPaint = new Paint(Paint.DITHER_FLAG);

	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		// view given size
		super.onSizeChanged(w, h, oldw, oldh);
		wid = w;
		ht = h;
		canvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		drawCanvas = new Canvas(canvasBitmap);
		drawPath.moveTo(0, 0);
		drawPath.lineTo(0, ht);
		drawPath.moveTo(0, ht);
		drawPath.lineTo(wid, ht);
		drawPath.moveTo(wid, ht);
		drawPath.lineTo(wid, 0);
		drawPath.moveTo(wid, 0);
		drawPath.lineTo(0, 0);
		drawCanvas.drawPath(drawPath, drawPaint);
		drawPath.reset();
		invalidate();
		if (MainActivity.control == 1) {
			BluetoothActivity.mCommandService
					.write(("Height " + h + " Width " + w).getBytes());// dun
																		// change
																		// constants
			int code = BluetoothActivity.mCommandService.readDataInt();
			if (code == Controls.PANEL_LAUNCH_FAIL) {
				System.out.println("hun kive handle karie?");
			} else if (code == Controls.SUCCESS) {
				System.out.println("bhalle bhalle");
			} else {
				System.out.println("thale thale");
			}
		}
	}

	public Bitmap getCanvasBitmap() {
		return canvasBitmap;
	}

	
	@Override
	protected void onDraw(Canvas canvas) {
		canvas.drawBitmap(canvasBitmap, 0, 0, canvasPaint);
		if (erase) {

			drawPaint.setXfermode(new PorterDuffXfermode(
					android.graphics.PorterDuff.Mode.CLEAR));
			canvas.drawPath(drawPath, drawPaint);
			
		} else if (confirm) {
			drawPaint.setXfermode(null);
			edit.setBackgroundColor(Color.TRANSPARENT);
			edit.setTextColor(paintColor);
			edit.setDrawingCacheEnabled(true);
			textBitmap = edit.getDrawingCache();
			if (textBitmap != null) {
				drawCanvas.drawBitmap(textBitmap, x, y, drawPaint);

			}
			DrawActivity.textBtn.setBackgroundColor(Color.LTGRAY);
			DrawActivity.drawBtn.setBackgroundColor(Color.GRAY);

		} else {
			drawPaint.setXfermode(null);
			canvas.drawPath(drawPath, drawPaint);

		}

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {

		// detect user touch
		float touchX = event.getX();
		float touchY = event.getY();
		switch (event.getActionMasked()) {
		case MotionEvent.ACTION_DOWN:
			if (forTextBox) {
				x = touchX;
				y = touchY;
				confirm = true;
			} else {
				drawPath.moveTo(touchX, touchY);
				oldX = touchX;
				oldY = touchY;
			}
			break;
		case MotionEvent.ACTION_MOVE:
			if (forTextBox) {

			} else
				drawPath.lineTo(touchX, touchY);
			break;
		case MotionEvent.ACTION_UP:
			if (forTextBox) {
				forTextBox = false;
				confirm = false;
			} else {
				if (oldX == touchX && oldY == touchY) {
					// to make dots also
					drawPath.lineTo(touchX + 1, touchY + 1);
				}
				drawCanvas.drawPath(drawPath, drawPaint);
				drawPath.reset();
			}
			break;
		default:
			return false;
		}

		invalidate();
		return true;
	}

	

	public void setColorInt(int newColor) {
		invalidate();

		paintColor = newColor;
		drawPaint.setColor(paintColor);
	}

	public void setBrushSize(float newSize) {
		// update size
		float pixelAmount = TypedValue.applyDimension(
				TypedValue.COMPLEX_UNIT_DIP, newSize, getResources()
						.getDisplayMetrics());
		brushSize = pixelAmount;
		drawPaint.setStrokeWidth(brushSize);
	}

	public void setLastBrushSize(float lastSize) {
		lastBrushSize = lastSize;
	}

	public float getLastBrushSize() {
		return lastBrushSize;
	}

	public void setErase(boolean isErase) {
		erase = isErase;
		if (erase) {
			
			paintColor = drawPaint.getColor();

		} else {
			drawPaint.setColor(paintColor);
			setBrushSize(getLastBrushSize());
		}

	}

	public void startNew() {
		canvasBitmap = Bitmap.createBitmap(wid, ht, Bitmap.Config.ARGB_8888);
		drawCanvas = new Canvas(canvasBitmap);

		brushSize = getResources().getInteger(R.integer.medium_size);
		erase = false;
		lastBrushSize = brushSize;
		drawPaint.setXfermode(null);
		paintColor = 0xFF660000;
		drawPaint.setColor(paintColor);
		setBrushSize(getLastBrushSize());
		drawPath.moveTo(0, 0);
		drawPath.lineTo(0, ht);
		drawPath.moveTo(0, ht);
		drawPath.lineTo(wid, ht);
		drawPath.moveTo(wid, ht);
		drawPath.lineTo(wid, 0);
		drawPath.moveTo(wid, 0);
		drawPath.lineTo(0, 0);
		drawCanvas.drawPath(drawPath, drawPaint);
		drawPath.reset();

		invalidate();
	}

	public void open(Bitmap bitmap) {
		System.out.println(ht + "    " + bitmap.getHeight());
		System.out.println(wid + "   " + bitmap.getWidth());

		canvasBitmap = Bitmap.createBitmap(bitmap);
		System.out.println(canvasBitmap.isMutable());
		drawCanvas = new Canvas(canvasBitmap);
		drawPath.moveTo(0, 0);
		drawPath.lineTo(0, ht);
		drawPath.moveTo(0, ht);
		drawPath.lineTo(wid, ht);
		drawPath.moveTo(wid, ht);
		drawPath.lineTo(wid, 0);
		drawPath.moveTo(wid, 0);
		drawPath.lineTo(0, 0);
		drawCanvas.drawPath(drawPath, drawPaint);
		drawPath.reset();
		drawPaint.setXfermode(null);
		brushSize = getResources().getInteger(R.integer.medium_size);
		lastBrushSize = brushSize;
		drawPath = new Path();
		drawPaint = new Paint();

		paintColor = 0xFF660000;
		drawPaint.setColor(paintColor);

		drawPaint.setAntiAlias(true);
		drawPaint.setStrokeWidth(brushSize);// medium
		drawPaint.setStyle(Paint.Style.STROKE);
		drawPaint.setStrokeJoin(Paint.Join.ROUND);
		drawPaint.setStrokeCap(Paint.Cap.ROUND);

		canvasPaint = new Paint(Paint.DITHER_FLAG);

		invalidate();
	}
}
