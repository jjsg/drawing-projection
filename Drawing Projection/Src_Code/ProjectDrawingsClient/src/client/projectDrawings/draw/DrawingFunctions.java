package client.projectDrawings.draw;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FilenameFilter;

import yuku.ambilwarna.AmbilWarnaDialog;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import client.projectDrawings.DC.DirectoryChooserDialog;
import client.projectDrawings.extraStrings.Controls;
import client.projectDrawings.extraStrings.ExtraStringConstants;

public class DrawingFunctions {

	public static DrawActivity context;

	/**
	 * Diplaying fragment view for selected nav drawer list item
	 * */
	public static void displayView(int position) {
		// update the main content by replacing fragments
		
		switch (position) {
		case 0:
			newFunction();
			break;
		case 1:
			context.openFunction();
			break;
		case 2:
			saveFunction();
			break;
		case 3:
			colorPicker();
			break;
		case 4:
			submitFunction(1);
			break;
		case 5:
			 Controls.quitServer=false;
	         context.setResult(Activity.RESULT_CANCELED);
	         context.finish();
			break;
		case 6:
			 Controls.quitServer=true;
	         context.setResult(Activity.RESULT_CANCELED);
	         context.finish();
			break;
		default:
			break;
		}
		DrawActivity.mDrawerList.setItemChecked(position, true);
		DrawActivity.mDrawerList.setSelection(position);
		context.setTitle(DrawActivity.navMenuTitles[position]);
		DrawActivity.mDrawerLayout.closeDrawer(DrawActivity.mDrawerList);
	}

	public static void submitFunction(int num) {
		
		DrawActivity.submitBtn.setBackgroundColor(Color.GRAY);
		DrawActivity.drawBtn.setBackgroundColor(Color.LTGRAY);
		DrawActivity.eraseBtn.setBackgroundColor(Color.LTGRAY);
		DrawActivity.drawMenuBtn.setBackgroundColor(Color.LTGRAY);
		DrawActivity.previousMenuBtn.setBackgroundColor(Color.LTGRAY);
		DrawActivity.nextMenuBtn.setBackgroundColor(Color.LTGRAY);
		DrawActivity.textBtn.setBackgroundColor(Color.LTGRAY);
		
		if (MainActivity.control == 1) {
			BluetoothActivity.mCommandService.write("<START>".getBytes());// dun
																			// change
																			// constants
			int code = BluetoothActivity.mCommandService.readDataInt();
			if (code == Controls.SUCCESS) {
				try {
					System.out.println("bhalle bhalle");
					DrawActivity.drawView.setDrawingCacheEnabled(true);
					Bitmap bm = DrawActivity.drawView.getDrawingCache();
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
					byte[] imageBytes = baos.toByteArray();
					String encodedImage = Base64.encodeToString(imageBytes,
							Base64.DEFAULT);
					BluetoothActivity.mCommandService.write(encodedImage
							.getBytes());// dun change constants
					BluetoothActivity.mCommandService
							.write(("<END" + num + ">").getBytes());// dun
																	// change
																	// constants
					DrawActivity.drawView.setDrawingCacheEnabled(false);
					code = BluetoothActivity.mCommandService.readDataInt();
					if (code == Controls.SUCCESS && num == 1) {
						Toast.makeText(context, "Saved successfully on server",
								Toast.LENGTH_LONG).show();
					} else if (code == Controls.FAIL_SAVE_PROJECT && num == 1) {
						Toast.makeText(context, "Unable to save on server",
								Toast.LENGTH_LONG).show();
					}
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("oopppps");
				}
			} else {
				System.out.println("thale thale");
			}
		}
		DrawActivity.submitBtn.setBackgroundColor(Color.LTGRAY);
		if (DrawActivity.drawView.isErase())
			DrawActivity.eraseBtn.setBackgroundColor(Color.GRAY);
		else
			DrawActivity.drawBtn.setBackgroundColor(Color.GRAY);
		DrawActivity.drawMenuBtn.setBackgroundColor(Color.LTGRAY);
		DrawActivity.previousMenuBtn.setBackgroundColor(Color.LTGRAY);
		DrawActivity.nextMenuBtn.setBackgroundColor(Color.LTGRAY);
		DrawActivity.textBtn.setBackgroundColor(Color.LTGRAY);
		context.setTitle(DrawActivity.mDrawerTitle);
	}

	public static void drawFunction() {
		final Dialog brushDialog = new Dialog(context);
		brushDialog.setTitle("Draw Brush size:");
		brushDialog.setContentView(R.layout.brush_chooser);

		ImageButton smallBtn = (ImageButton) brushDialog
				.findViewById(R.id.small_brush);
		smallBtn.setBackgroundColor(Color.LTGRAY);
		smallBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				View ov = ((ImageButton) brushDialog
						.findViewById(DrawActivity.originalBrushSizeId));
				drawOnClick(v, ov, DrawActivity.smallBrush);
				brushDialog.dismiss();
			}
		});

		ImageButton mediumBtn = (ImageButton) brushDialog
				.findViewById(R.id.medium_brush);
		mediumBtn.setBackgroundColor(Color.LTGRAY);
		mediumBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				View ov = ((ImageButton) brushDialog
						.findViewById(DrawActivity.originalBrushSizeId));
				drawOnClick(v, ov, DrawActivity.mediumBrush);
				brushDialog.dismiss();
			}
		});

		ImageButton largeBtn = (ImageButton) brushDialog
				.findViewById(R.id.large_brush);
		largeBtn.setBackgroundColor(Color.LTGRAY);
		largeBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				View ov = ((ImageButton) brushDialog
						.findViewById(DrawActivity.originalBrushSizeId));
				drawOnClick(v, ov, DrawActivity.largeBrush);
				brushDialog.dismiss();

			}
		});

		if (DrawActivity.originalBrushSizeId == 0)
			DrawActivity.originalBrushSizeId = mediumBtn.getId();

		((ImageButton) brushDialog
				.findViewById(DrawActivity.originalBrushSizeId))
				.setBackgroundColor(Color.GRAY);// change color
		// show dialog
		brushDialog.show();
	}

	private static void drawOnClick(View nv, View ov, float brush_size) {
		DrawActivity.drawView.setBrushSize(brush_size);
		DrawActivity.drawView.setLastBrushSize(brush_size);

		ov.setBackgroundColor(Color.LTGRAY);// change color
		DrawActivity.originalBrushSizeId = nv.getId();
		nv.setBackgroundColor(Color.GRAY);// change color

		DrawActivity.drawView.setErase(false);
		DrawActivity.submitBtn.setBackgroundColor(Color.LTGRAY);
		DrawActivity.drawBtn.setBackgroundColor(Color.GRAY);
		DrawActivity.eraseBtn.setBackgroundColor(Color.LTGRAY);
		DrawActivity.drawMenuBtn.setBackgroundColor(Color.LTGRAY);
		DrawActivity.previousMenuBtn.setBackgroundColor(Color.LTGRAY);
		DrawActivity.nextMenuBtn.setBackgroundColor(Color.LTGRAY);
		DrawActivity.textBtn.setBackgroundColor(Color.LTGRAY);

	}

	public static void eraseFunction() {
		// switch to erase - choose size

		final Dialog brushDialog = new Dialog(context);
		brushDialog.setTitle("Erase Brush size:");
		brushDialog.setContentView(R.layout.brush_chooser_erase);

		ImageButton smallBtn = (ImageButton) brushDialog
				.findViewById(R.id.small_brush);
		smallBtn.setBackgroundColor(Color.LTGRAY);
		smallBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				eraseOnClick(v, DrawActivity.smallBrush);
				brushDialog.dismiss();

			}
		});

		ImageButton mediumBtn = (ImageButton) brushDialog
				.findViewById(R.id.medium_brush);
		mediumBtn.setBackgroundColor(Color.LTGRAY);
		mediumBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				eraseOnClick(v, DrawActivity.mediumBrush);
				brushDialog.dismiss();

			}
		});

		ImageButton largeBtn = (ImageButton) brushDialog
				.findViewById(R.id.large_brush);
		largeBtn.setBackgroundColor(Color.LTGRAY);
		largeBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				eraseOnClick(v, DrawActivity.largeBrush);
				brushDialog.dismiss();
			}
		});

		ImageButton largestBtn = (ImageButton) brushDialog
				.findViewById(R.id.largest_brush);
		largestBtn.setBackgroundColor(Color.LTGRAY);
		largestBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				eraseOnClick(v, DrawActivity.largestBrush);
				brushDialog.dismiss();
			}
		});

		// show dialog
		brushDialog.show();

	}

	private static void eraseOnClick(View nv, float brush_size) {
		DrawActivity.drawView.setBrushSize(brush_size);
		nv.setBackgroundColor(Color.GRAY);// change color

		DrawActivity.drawView.setErase(true);
		DrawActivity.submitBtn.setBackgroundColor(Color.LTGRAY);
		DrawActivity.drawBtn.setBackgroundColor(Color.LTGRAY);
		DrawActivity.eraseBtn.setBackgroundColor(Color.GRAY);
		DrawActivity.drawMenuBtn.setBackgroundColor(Color.LTGRAY);
		DrawActivity.previousMenuBtn.setBackgroundColor(Color.LTGRAY);
		DrawActivity.nextMenuBtn.setBackgroundColor(Color.LTGRAY);
		DrawActivity.textBtn.setBackgroundColor(Color.LTGRAY);

	}

	public static void newFunction() {
		// new button
		AlertDialog.Builder newDialog = new AlertDialog.Builder(context);
		newDialog.setTitle("New drawing");
		newDialog
				.setMessage("Start new drawing (you will lose the current drawing)?");
		newDialog.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						DrawActivity.drawView.startNew();

						DrawActivity.submitBtn.setBackgroundColor(Color.LTGRAY);
						DrawActivity.drawBtn.setBackgroundColor(Color.GRAY);
						DrawActivity.eraseBtn.setBackgroundColor(Color.LTGRAY);
						DrawActivity.drawMenuBtn
								.setBackgroundColor(Color.LTGRAY);
						DrawActivity.previousMenuBtn
								.setBackgroundColor(Color.LTGRAY);
						DrawActivity.nextMenuBtn.setBackgroundColor(Color.LTGRAY);
						DrawActivity.textBtn.setBackgroundColor(Color.LTGRAY);
						dialog.dismiss();
						context.setTitle(DrawActivity.mDrawerTitle);
					}
				});

		newDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
						context.setTitle(DrawActivity.mDrawerTitle);
					}
				});
		newDialog.show();
		
	}

	public static void saveFunction() {

		// save drawing
		AlertDialog.Builder saveDialog = new AlertDialog.Builder(context);
		saveDialog.setTitle("Save drawing");
		saveDialog.setMessage("Save drawing to device Gallery?");
		saveDialog.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					private String m_chosenDir = "";
					private boolean m_newFolderEnabled = true;

					public void onClick(DialogInterface dialog, int which) {
						// Create DirectoryChooserDialog and register a callback
						DirectoryChooserDialog directoryChooserDialog = new DirectoryChooserDialog(
								context,
								new DirectoryChooserDialog.ChosenDirectoryListener() {
									@Override
									public void onChosenDir(String chosenDir) {
										m_chosenDir = chosenDir;
										Toast.makeText(
												context,
												"Chosen directory: "
														+ chosenDir,
												Toast.LENGTH_LONG).show();
										saveImage(chosenDir);
										context.setTitle(DrawActivity.mDrawerTitle);
									}
								});

						// Toggle new folder button enabling
						directoryChooserDialog
								.setNewFolderEnabled(m_newFolderEnabled);
						// Load directory chooser dialog for initial
						// 'm_chosenDir' directory.
						// The registered callback will be called upon final
						// directory selection.
						directoryChooserDialog.chooseDirectory(m_chosenDir);
						m_newFolderEnabled = !m_newFolderEnabled;
					}

				});
		saveDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
						context.setTitle(DrawActivity.mDrawerTitle);
					}
				});
		saveDialog.show();
	}

	public static void saveImage(String chosenDir) {
		// save drawing

		Intent saveIntent = new Intent(context, SaveFileNameActivity.class);
		saveIntent.putExtra(ExtraStringConstants.EXTRA_CHOSEN_DIR, chosenDir);
		context.startActivityForResult(saveIntent,
				DrawActivity.REQUEST_FILE_NAME);
	}

	public static void loadFileList() {
		try {
			DrawActivity.path.mkdirs();
		} catch (SecurityException e) {
			e.printStackTrace();
			Log.e(DrawActivity.TAG, "unable to write on the sd card ");
		}

		// Checks whether path exists
		if (DrawActivity.path.exists()) {
			FilenameFilter filter = new FilenameFilter() {
				@Override
				public boolean accept(File dir, String filename) {
					File sel = new File(dir, filename);
					// Filters based on whether the file is hidden or not
					return (sel.isFile() || sel.isDirectory())
							&& !sel.isHidden();

				}
			};

			String[] fList = DrawActivity.path.list(filter);
			DrawActivity.fileList = new Item[fList.length];
			for (int i = 0; i < fList.length; i++) {
				DrawActivity.fileList[i] = new Item(fList[i],
						R.drawable.file_icon);

				// Convert into file path
				File sel = new File(DrawActivity.path, fList[i]);

				// Set drawables
				if (sel.isDirectory()) {
					DrawActivity.fileList[i].icon = R.drawable.directory_icon;
					Log.d("DIRECTORY", DrawActivity.fileList[i].file);
				} else {
					Log.d("FILE", DrawActivity.fileList[i].file);
				}
			}

			if (!DrawActivity.firstLvl) {
				Item temp[] = new Item[DrawActivity.fileList.length + 1];
				for (int i = 0; i < DrawActivity.fileList.length; i++) {
					temp[i + 1] = DrawActivity.fileList[i];
				}
				temp[0] = new Item("Up", R.drawable.directory_up);
				DrawActivity.fileList = temp;
			}
		} else {
			Log.e(DrawActivity.TAG, "path does not exist");
		}

		DrawActivity.adapter = new ArrayAdapter<Item>(context,
				android.R.layout.select_dialog_item, android.R.id.text1,
				DrawActivity.fileList) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				// creates view
				View view = super.getView(position, convertView, parent);
				TextView textView = (TextView) view
						.findViewById(android.R.id.text1);

				// put the image on the text view
				textView.setCompoundDrawablesWithIntrinsicBounds(
						DrawActivity.fileList[position].icon, 0, 0, 0);

				// add margin between image and text (support various screen
				// densities)
				int dp5 = (int) (5 * context.getResources().getDisplayMetrics().density + 0.5f);
				textView.setCompoundDrawablePadding(dp5);

				return view;
			}
		};

	}

	public static void performFCAction(String image) {

		System.out.println(image);
		File imageFile = new File(image);
		if (imageFile.exists()) {
			DrawActivity.drawView.setErase(false);
			Bitmap newBitmap = BitmapFactory.decodeFile(imageFile
					.getAbsolutePath());
			Bitmap mutableBitmap = newBitmap
					.copy(Bitmap.Config.ARGB_8888, true);
			DrawActivity.drawView.open(mutableBitmap);

			DrawActivity.submitBtn.setBackgroundColor(Color.LTGRAY);
			DrawActivity.drawBtn.setBackgroundColor(Color.GRAY);
			DrawActivity.eraseBtn.setBackgroundColor(Color.LTGRAY);
			DrawActivity.drawMenuBtn.setBackgroundColor(Color.LTGRAY);
			DrawActivity.previousMenuBtn.setBackgroundColor(Color.LTGRAY);
			DrawActivity.nextMenuBtn.setBackgroundColor(Color.LTGRAY);
			DrawActivity.textBtn.setBackgroundColor(Color.LTGRAY);
			
		}
		context.setTitle(DrawActivity.mDrawerTitle);
	}

	public static void menuFunction(int c) {
		if (MainActivity.control == 1) {
			
			DrawActivity.submitBtn.setBackgroundColor(Color.LTGRAY);
			DrawActivity.drawBtn.setBackgroundColor(Color.LTGRAY);
			DrawActivity.eraseBtn.setBackgroundColor(Color.LTGRAY);
			DrawActivity.textBtn.setBackgroundColor(Color.LTGRAY);
			
			BluetoothActivity.mCommandService.write(String.valueOf(c)
					.getBytes());// dun change constants
			BluetoothActivity.mCommandService.readDataInt();// dun change
															// constants
			DrawActivity.submitBtn.setBackgroundColor(Color.LTGRAY);
			if(DrawActivity.drawView.isErase())
			{
				DrawActivity.eraseBtn.setBackgroundColor(Color.GRAY);
			}
			else
			{
				DrawActivity.drawBtn.setBackgroundColor(Color.GRAY);
			}
			
		}
	}
	
	
	public static void textBox()
	{
		
		
		LayoutInflater layoutInflater=LayoutInflater.from(context);
    	
    	View promptView = layoutInflater.inflate(R.layout.prompt, null);
    	

		final TextView tv=(TextView) promptView.findViewById(R.id.heading);
		
		tv.setText("Enter the text (After ok, click where you want to put)");
		
		
		AlertDialog.Builder alertDialogBuilder= new AlertDialog.Builder(context);

		// set prompts.xml to be the layout file of the alertdialog builder
		alertDialogBuilder.setView(promptView);

		final EditText input = (EditText) promptView.findViewById(R.id.userInput);
		// setup a dialog window
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// get user input and set it to result
								DrawActivity.drawView.edit=input;
								DrawActivity.drawView.forTextBox=true;
								tv.setText(input.getText());
								
								DrawActivity.drawView.setErase(false);
								DrawActivity.submitBtn.setBackgroundColor(Color.LTGRAY);
								DrawActivity.drawBtn.setBackgroundColor(Color.LTGRAY);
								DrawActivity.eraseBtn.setBackgroundColor(Color.LTGRAY);
								DrawActivity.drawMenuBtn.setBackgroundColor(Color.LTGRAY);
								DrawActivity.previousMenuBtn.setBackgroundColor(Color.LTGRAY);
								DrawActivity.nextMenuBtn.setBackgroundColor(Color.LTGRAY);
								DrawActivity.textBtn.setBackgroundColor(Color.GRAY);
								dialog.cancel();
							}
						})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,	int id) {
								tv.setText("");
								DrawActivity.drawView.forTextBox=false;
								dialog.cancel();
							}
						});

		// create an alert dialog
		AlertDialog alertD = alertDialogBuilder.create();

		alertD.show();
		
	}
	

	public static void colorPicker()
	{
		AmbilWarnaDialog dialog = new AmbilWarnaDialog(context,DrawActivity.drawView.paintColor, new AmbilWarnaDialog.OnAmbilWarnaListener() {
			@Override
			public void onOk(AmbilWarnaDialog dialog, int color) {
				DrawActivity.drawView.setColorInt(color);
				DrawActivity.drawView.setErase(false);
				DrawActivity.submitBtn.setBackgroundColor(Color.LTGRAY);
				DrawActivity.drawBtn.setBackgroundColor(Color.GRAY);
				DrawActivity.eraseBtn.setBackgroundColor(Color.LTGRAY);
				DrawActivity.drawMenuBtn.setBackgroundColor(Color.LTGRAY);
				DrawActivity.previousMenuBtn.setBackgroundColor(Color.LTGRAY);
				DrawActivity.nextMenuBtn.setBackgroundColor(Color.LTGRAY);
				DrawActivity.textBtn.setBackgroundColor(Color.LTGRAY);
				context.setTitle(DrawActivity.mDrawerTitle);
			}

			@Override
			public void onCancel(AmbilWarnaDialog dialog) {
				context.setTitle(DrawActivity.mDrawerTitle);
			}
		});
		dialog.show();
	}

}

/**
 * Slide menu item click listener
 * */
class SlideMenuClickListener implements ListView.OnItemClickListener {
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// display view for selected nav drawer item
		DrawingFunctions.displayView(position);
	}
}



class Item {
	public String file;
	public int icon;

	public Item(String file, Integer icon) {
		this.file = file;
		this.icon = icon;
	}

	@Override
	public String toString() {
		return file;
	}
}


