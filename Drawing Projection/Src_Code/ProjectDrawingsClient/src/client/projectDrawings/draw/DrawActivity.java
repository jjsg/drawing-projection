package client.projectDrawings.draw;

import info.androidhive.slidingmenu.adapter.NavDrawerListAdapter;
import info.androidhive.slidingmenu.model.NavDrawerItem;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import client.projectDrawings.extraStrings.Controls;
import client.projectDrawings.extraStrings.ExtraStringConstants;
import client.projectDrawings.view.DrawingView;

public class DrawActivity extends ActionBarActivity implements OnClickListener
{
	
	public static DrawingView drawView;
	
	public static ImageButton drawBtn, eraseBtn,submitBtn,textBtn;
	public static ImageButton previousMenuBtn,nextMenuBtn,drawMenuBtn;
	
	public static int originalBrushSizeId=0;
	
	public static float smallBrush, mediumBrush, largeBrush,largestBrush;
	
	//FC variable starts
	// Stores names of traversed directories
	ArrayList<String> str = new ArrayList<String>();

	// Check if the first level of the directory structure is the one showing
	public static Boolean firstLvl = true;
	public static Item[] fileList;
	public static File path = new File(Environment.getExternalStorageDirectory() + "");
	public static String chosenFile;
	public static final int DIALOG_LOAD_FILE = 1000;
	public static ListAdapter adapter;
	public static final String TAG = "F_PATH";
	//FC variables end
	
	//save
	public static final int REQUEST_FILE_NAME=1;
	
	//textbox
	public static String textBox; 
	
	
	//sliding menu declaration requirement
	public static DrawerLayout mDrawerLayout;
	public static ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	// nav drawer title
	public static CharSequence mDrawerTitle;

	// used to store app title
	private CharSequence mTitle;

	// slide menu items
	public static String[] navMenuTitles;
	private TypedArray navMenuIcons;

	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter menuAdapter;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		requestWindowFeature(Window.FEATURE_ACTION_BAR);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_draw);
		

		DrawingFunctions.context=this;		
				
		drawBtn = (ImageButton)findViewById(R.id.draw_btn);
		drawBtn.setOnClickListener(this);
				
		smallBrush = getResources().getInteger(R.integer.small_size);
		mediumBrush = getResources().getInteger(R.integer.medium_size);
		largeBrush = getResources().getInteger(R.integer.large_size);
		largestBrush = getResources().getInteger(R.integer.largest_size);
		
		drawView = (DrawingView)findViewById(R.id.drawing);
		drawView.setBrushSize(mediumBrush);
		
		eraseBtn = (ImageButton)findViewById(R.id.erase_btn);
		eraseBtn.setOnClickListener(this);
		
		textBtn = (ImageButton)findViewById(R.id.text_btn);
		textBtn.setOnClickListener(this);
		
		submitBtn = (ImageButton)findViewById(R.id.submit_btn);
		submitBtn.setOnClickListener(this);
		
		previousMenuBtn = (ImageButton)findViewById(R.id.previousMenuBtn);
		previousMenuBtn.setOnClickListener(this);
		
		nextMenuBtn = (ImageButton)findViewById(R.id.nextMenuBtn);
		nextMenuBtn.setOnClickListener(this);
		
		drawMenuBtn = (ImageButton)findViewById(R.id.drawMenuBtn);
		drawMenuBtn.setOnClickListener(this);
		

	
		drawBtn.setBackgroundColor(Color.GRAY);
		eraseBtn.setBackgroundColor(Color.LTGRAY);
		textBtn.setBackgroundColor(Color.LTGRAY);
		submitBtn.setBackgroundColor(Color.LTGRAY);
		drawMenuBtn.setBackgroundColor(Color.LTGRAY);
		previousMenuBtn.setBackgroundColor(Color.LTGRAY);
		nextMenuBtn.setBackgroundColor(Color.LTGRAY);
		
		if(MainActivity.control==0)
		{
            submitBtn.setVisibility(View.GONE);
            previousMenuBtn.setVisibility(View.GONE);
            drawMenuBtn.setVisibility(View.GONE);
            nextMenuBtn.setVisibility(View.GONE);
            
		}
		
		slidingMenuInitialization(savedInstanceState);
	}
	
	


	private void slidingMenuInitialization(Bundle savedInstanceState) {
		
		mTitle = mDrawerTitle = getTitle();

		// load slide menu items
		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

		// nav drawer icons from resources
		navMenuIcons = getResources()
				.obtainTypedArray(R.array.nav_drawer_icons);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

		navDrawerItems = new ArrayList<NavDrawerItem>();

		// adding nav drawer items to array
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));
		
		if(MainActivity.control!=0)
		{
			navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));
			navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1)));
			navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons.getResourceId(6, -1)));
		}
		

		// Recycle the typed array
		navMenuIcons.recycle();
		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());
		menuAdapter = new NavDrawerListAdapter(getApplicationContext(),navDrawerItems);
		mDrawerList.setAdapter(menuAdapter);

		// enabling action bar app icon and behaving it as toggle button
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
		getSupportActionBar().setTitle(Html.fromHtml("<font color=\"black\">" + getString(R.string.app_name) + "</font>"));
		
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, //nav menu toggle icon
				R.string.app_name, // nav drawer open - description for accessibility
				R.string.app_name // nav drawer close - description for accessibility
		) {
			public void onDrawerClosed(View view) {
				System.out.println("Closed");
				setTitle(mTitle);
				
				mDrawerList.clearChoices();
				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				System.out.println("Opened");
				setTitle(mDrawerTitle);
				// calling onPrepareOptionsMenu() to hide action bar icons
				invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);
	}
	
	
	/* *
	 * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		return super.onPrepareOptionsMenu(menu);
	}

	
	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		mTitle=Html.fromHtml("<font color=\"black\">" + title + "</font>");
		getSupportActionBar().setTitle(mTitle);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}


	@Override
	public void onClick(View view)
	{
		
		//respond to clicks  
		if(view.getId()==R.id.draw_btn)
		{
			DrawingFunctions.drawFunction();
		}
		else if(view.getId()==R.id.erase_btn)
		{
		    DrawingFunctions.eraseFunction();
		}
		else if(view.getId()==R.id.submit_btn)
		{			
			DrawingFunctions.submitFunction(0);
		}
		else if(view.getId()==R.id.nextMenuBtn)
		{	
			nextMenuBtn.setBackgroundColor(Color.GRAY);
			previousMenuBtn.setBackgroundColor(Color.LTGRAY);
			drawMenuBtn.setBackgroundColor(Color.LTGRAY);
			DrawingFunctions.menuFunction(Controls.NEXT);
			nextMenuBtn.setBackgroundColor(Color.LTGRAY);
			
		}
		else if(view.getId()==R.id.previousMenuBtn)
		{			
			previousMenuBtn.setBackgroundColor(Color.GRAY);
			nextMenuBtn.setBackgroundColor(Color.LTGRAY);
			drawMenuBtn.setBackgroundColor(Color.LTGRAY);
			DrawingFunctions.menuFunction(Controls.PREVIOUS);
			previousMenuBtn.setBackgroundColor(Color.LTGRAY);
		}
		else if(view.getId()==R.id.drawMenuBtn)
		{	
			drawMenuBtn.setBackgroundColor(Color.GRAY);
			previousMenuBtn.setBackgroundColor(Color.LTGRAY);
			nextMenuBtn.setBackgroundColor(Color.LTGRAY);
			DrawingFunctions.menuFunction(Controls.DRAW);
			drawMenuBtn.setBackgroundColor(Color.LTGRAY);
		}
		else if(view.getId()==R.id.text_btn)
		{	
			DrawingFunctions.textBox();
		}
			
	}
	
	
	
	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
        case REQUEST_FILE_NAME:
            // When saveFileName returns with a name to save with
            if (resultCode == Activity.RESULT_OK) 
            {
                String chosenDir = data.getExtras().getString(ExtraStringConstants.EXTRA_CHOSEN_DIR);
                String imgName=data.getExtras().getString(ExtraStringConstants.EXTRA_CHOSEN_FILENAME)+".png";
                drawView.setDrawingCacheEnabled(true);
                Bitmap bm= drawView.getDrawingCache();
			    File f = new File(chosenDir,imgName);
			    OutputStream fOut = null;
			    try 
			    {
			    	fOut = new FileOutputStream(f);
			    	/**Compress image**/
			    	bm.compress(Bitmap.CompressFormat.PNG, 85, fOut);
			    	fOut.flush();
			    	fOut.close();
		       	    
			    	/**Update image to gallery**/
			    	String imgSaved=  MediaStore.Images.Media.insertImage(getContentResolver(),f.getAbsolutePath(), f.getName(), f.getName());
			    	if(imgSaved!=null)
			    	{
					    Toast savedToast = Toast.makeText(getApplicationContext(), "Drawing saved at "+chosenDir+"/"+imgName, Toast.LENGTH_SHORT);
					    savedToast.show();
			    	}
			       else
			       {
				   	    Toast unsavedToast = Toast.makeText(getApplicationContext(),  "Oops! Image could not be saved,consider taking screen snapshot", Toast.LENGTH_SHORT);
				    	unsavedToast.show();
				    }
				    drawView.destroyDrawingCache();
			    }
			    catch (Exception e) {
			    	e.printStackTrace();
			        Toast unsavedToast = Toast.makeText(getApplicationContext(),
				    	        "Oops! Image could not be saved,consider taking screen snapshot", Toast.LENGTH_SHORT);
				   	unsavedToast.show();
			    }	
            }
            else
            {
                Toast unsavedToast = Toast.makeText(getApplicationContext(),
		    	        "Oops! save cancelled", Toast.LENGTH_SHORT);
                unsavedToast.show();
            }
            break;
        }
            	
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		if (BluetoothActivity.mCommandService != null)
		{
			BluetoothActivity.mCommandService.stop();
			BluetoothActivity.mCommandService=null;
		}
			
	}
	
	
	public void openFunction()
	{
		DrawingFunctions.loadFileList();
		showDialog(DIALOG_LOAD_FILE);	
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog dialog = null;
		AlertDialog.Builder builder = new Builder(this);

		if (fileList == null) {
			Log.e(TAG, "No files loaded");
			dialog = builder.create();
			return dialog;
		}

		switch (id) {
		
		case DIALOG_LOAD_FILE:
			builder.setTitle("Choose your file");
			builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					chosenFile = fileList[which].file;
					File sel = new File(path + "/" + chosenFile);
					if (sel.isDirectory()) {
						firstLvl = false;

						// Adds chosen directory to list
						str.add(chosenFile);
						fileList = null;
						path = new File(sel + "");

						DrawingFunctions.loadFileList();

						removeDialog(DIALOG_LOAD_FILE);
						showDialog(DIALOG_LOAD_FILE);
						Log.d(TAG, path.getAbsolutePath());

					}

					// Checks if 'up' was clicked
					else if (chosenFile.equalsIgnoreCase("up") && !sel.exists()) {

						// present directory removed from list
						String s = str.remove(str.size() - 1);

						// path modified to exclude present directory
						path = new File(path.toString().substring(0,
								path.toString().lastIndexOf(s)));
						fileList = null;

						// if there are no more directories in the list, then
						// its the first level
						if (str.isEmpty()) {
							firstLvl = true;
						}
						DrawingFunctions.loadFileList();

						removeDialog(DIALOG_LOAD_FILE);
						showDialog(DIALOG_LOAD_FILE);
						Log.d(TAG, path.getAbsolutePath());

					}
					// File picked
					else {
						
						// Perform action with file picked
						// Intent FCIntent = new Intent();
						// FCIntent.putExtra(ExtraStringConstants.EXTRA_FC_PATH, path+"/"+chosenFile);
				        // setResult(Activity.RESULT_OK, FCIntent);  
				        // finish();
				         DrawingFunctions.performFCAction(path+"/"+chosenFile);
					}

				}
				
			});
			break;
		}
		dialog = builder.show();
		return dialog;
	}
	
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
		
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
		
		if(mDrawerToggle.onOptionsItemSelected(item)){
			return true;
		}
			return true;
    }
	
	

	


}

