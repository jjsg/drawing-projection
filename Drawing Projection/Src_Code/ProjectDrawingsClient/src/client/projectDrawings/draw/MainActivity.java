package client.projectDrawings.draw;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends Activity {
	
	 
	
	public static int control=0;//0 noraml,1 bluetooth,2 wifi
	private static final String normal="Normal";
	//private static final String wifi="Project over wifi";
	private static final String bluetooth="Project over bluetooth";
	

	
	// The on-click listener for all controls in the ListViews
    private OnItemClickListener mControlClickListener = new OnItemClickListener()
    {
        public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3)
        {
          
            // Get the player_name
           String controlType = ((TextView) v).getText().toString();
           System.out.println(control);
         
           if(controlType.equals(normal))
       	   {
        	   control=0;
        	   Intent myIntent = new Intent(getBaseContext(), DrawActivity.class);
               startActivity(myIntent);
               //finish();
       	   }
           else if(controlType.equals(bluetooth))
       	   {
        	   control=1;
        	   Intent myIntent = new Intent(getBaseContext(), BluetoothActivity.class);
               startActivity(myIntent);
               finish();
       	   } 
           /*else if(controlType.equals(wifi))
       	   {
        	   control=2;
        	   Intent myIntent = new Intent(getBaseContext(), WifiActivity.class);
               startActivity(myIntent);
               finish();
       	   }
           */
           
            
       	   
        }
    };

	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
        
        String[] controlsArray={normal,bluetooth};
        // Find and set up the ListView for paired devices
        ListView availablePlayers = (ListView) findViewById(R.id.available_controls);
        ArrayAdapter adapter = new ArrayAdapter<String>(this,R.layout.activity_listview, controlsArray);
        availablePlayers.setAdapter(adapter);
        availablePlayers.setOnItemClickListener(mControlClickListener);
        
     
       
	}


}
