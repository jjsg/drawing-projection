package client.projectDrawings.draw;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import client.projectDrawings.extraStrings.ExtraStringConstants;

public class SaveFileNameActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_save_file_name);
	}
	public void saveButton(View view)
	{
		EditText saveName=(EditText)findViewById(R.id.saveName);
		Intent saveIntent = new Intent();
		saveIntent.putExtra(ExtraStringConstants.EXTRA_CHOSEN_FILENAME, saveName.getText().toString());
		saveIntent.putExtra(ExtraStringConstants.EXTRA_CHOSEN_DIR, getIntent().getExtras().getString(ExtraStringConstants.EXTRA_CHOSEN_DIR));
        setResult(Activity.RESULT_OK, saveIntent);  
        finish();	
	}

}
