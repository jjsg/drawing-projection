package client.projectDrawings.extraStrings;

public class ExtraStringConstants {
	
	
	// Return Intent extra from savedialogactivity
    public static String EXTRA_CHOSEN_DIR="chosenDir";
    public static String EXTRA_CHOSEN_FILENAME="filename";
    
  //Pass Intent from MainActivity
  	public static String EXTRA_RIGHT_TEXT = "right_text";
  	
  	
  	// Return Intent extra from DeviceListActivity
      public static String EXTRA_DEVICE_ADDRESS = "device_address";
      
     // Return Intent extra from SelectMediaPlayerActivity
      public static String EXTRA_SELECTED_PLAYER = "selected_player";
    
    
}
