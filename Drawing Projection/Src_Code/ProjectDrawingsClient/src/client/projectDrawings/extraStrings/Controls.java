package client.projectDrawings.extraStrings;


public class Controls {
	
	 // Constants that indicate command to computer
 //   public static final int EXIT_CMD = -1;
    public static final int SUCCESS=0;
    public static final int PANEL_LAUNCH_FAIL=1;
  //  public static final int EXIT_CMD_1=2;
    public static final int PREVIOUS=3;
    public static final int NEXT=4;
    public static final int DRAW=5;
    public static final int FAIL_SAVE_PROJECT=6;
    
    public static final int QUIT_APP=100;
    public static final int QUIT_SERVER=200;
    public static final int RESULT_QUIT=179;
    
    public static boolean quitServer=false;
    
    //  public static final int NEXT_PDF=7;
   // public static final int PREVIOUS_PDF=8;
   // public static boolean isPDF=false;
	
}
