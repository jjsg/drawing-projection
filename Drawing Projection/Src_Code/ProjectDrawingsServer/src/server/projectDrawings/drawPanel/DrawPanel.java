package server.projectDrawings.drawPanel;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import server.projectDrawings.bluetooth.ProcessConnectionThread;

public class DrawPanel extends JPanel {    
	
	public Graphics gObj;
	public Image image;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public DrawPanel() {
        setOpaque(false);
    }
	
	public DrawPanel(Image img)
	{
		setOpaque(false);
		this.image=img;
	}
	
	/*
	@Override
	public void update(Graphics g) {
		
		 super.update(g);
	        
	        if(image==null)
	        {
	      
		      
		    }
	        else
	        	g.drawImage(image,5,0,ProcessConnectionThread.scaleWidth,ProcessConnectionThread.scaleHeight,this);
	        	
	        System.out.println("pain com");
	};*/
	

	@Override
    public void paintComponent (Graphics g)     
    {
        super.paintComponent(g);
        
        if(image==null)
        {
      
	    }
        else
        	//g.drawImage(image, 0, 0, null);
        	g.drawImage(image,0,0,ProcessConnectionThread.scaleWidth,ProcessConnectionThread.scaleHeight,this);
        	
        //System.out.println("pain com");
        
        
    }
	/*
	@Override
	public void paint(Graphics g) 
	{
		  ///Graphics2D g2d = (Graphics2D) g.create();
          //g2d.setComposite(AlphaComposite.SrcOver.derive(0.85f));
          //g2d.setColor(getBackground());
          //g2d.fillRect(0, 0, getWidth(), getHeight());
		
	        if(image!=null){
	        
	        	g.drawImage(image,5,0,ProcessConnectionThread.scaleWidth,ProcessConnectionThread.scaleHeight,this);
	        }	
	       
		System.out.println("here");
	}
	*/
	@Override
	public void repaint() 
	{
	//	System.out.println("repaint");
		super.repaint();
	}


    public static void main(String[] args) throws InterruptedException 
    {
        JFrame frame = new JFrame("Points");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ImageIcon ic=new ImageIcon("test.png");
        frame.add(new DrawPanel(ic.getImage()));
        frame.setSize(900, 700);
        frame.setVisible(true);
        Thread.sleep(5000);
        ic=new ImageIcon("test1.png");
        frame.add(new DrawPanel(ic.getImage()));
        frame.setSize(900, 700);
        frame.setVisible(true);
     }
} 