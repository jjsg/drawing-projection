package server.projectDrawings.bluetooth;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Image;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.microedition.io.StreamConnection;
import javax.swing.JFrame;

import org.apache.commons.net.util.Base64;

import server.projectDrawings.drawPanel.DrawPanel;
import server.projectDrawings.extraStrings.Controls;

public class ProcessConnectionThread implements Runnable{

	private StreamConnection mConnection;
	int width,ht;
	private JFrame frame=null;
	int sleepTime=500;
	public static int scaleWidth,scaleHeight;
	public Image image=null;

	public ProcessConnectionThread(StreamConnection connection)
	{
		mConnection = connection;
	}
	
	@Override
	public void run() {
		try {
			
			// prepare to receive data
			InputStream inputStream = mConnection.openInputStream();
			OutputStream outputStream = mConnection.openOutputStream();
			System.out.println("waiting for input123");
			
		
	        
	        while (true)
	        {
	        	byte [] buffer=new byte[1024];
	        	int readBytes=inputStream.read(buffer);
	        	String c=new String(buffer,0,readBytes);
	        	System.out.println("CCCC"+c);
	        	int command=100;
	        	String cc[]=c.split(" ");
	        	try{
	        		
	        		
	        		if(cc[0].equals("Height"))
	        		{
	        			ht=Integer.parseInt(cc[1]);
	        			width=Integer.parseInt(cc[3]);
	        			if(launchPanel(new DrawPanel()))
	        			{
	        				outputStream.write(Controls.SUCCESS);
	        			}
	        			else
	        			{
	        				outputStream.write(Controls.PANEL_LAUNCH_FAIL);
	        			}
	        		}
	        		else if(c.startsWith("<START>"))
	        		{
	        			outputStream.write(Controls.SUCCESS);
	        			
	        			StringBuilder encodedImage=new StringBuilder();
	        			do
	        			{
	        				byte [] newbuffer=new byte[250000];
	        				readBytes=inputStream.read(newbuffer);
	        				c=new String(newbuffer,0,readBytes);
	        				encodedImage.append(c);
	        				
	        			}while(!(c.contains("<END0>")||c.contains("<END1>")));
	        			int chopIndex=encodedImage.indexOf("<END0>");
	        			boolean saveToDisk=false;
	        			if(chopIndex==-1)
	        			{
	        				chopIndex=encodedImage.indexOf("<END1>");
	        				saveToDisk=true;
	        			}
	        		//	System.out.println("chop"+chopIndex);
	        			String s=encodedImage.substring(0, chopIndex);
	        			byte[] decodedImage=Base64.decodeBase64(s);
	        			
	        			BufferedImage imag=ImageIO.read(new ByteArrayInputStream(decodedImage));
	        			try{
	        				if(saveToDisk)
	        				{
	        					ImageIO.write(imag, "png", new File(MainServer.saveLoc,"projected"+new Date()+".png"));
	        				}
	        				outputStream.write(Controls.SUCCESS);
	        			}
	        			catch(Exception e)
	        			{
	        				e.printStackTrace();
	        				System.out.println("invalid location");
	        				outputStream.write(Controls.FAIL_SAVE_PROJECT);
	        			}
	        		
	        	       
	        			
	        			image=imag;
	        			launchPanel(new DrawPanel(imag));
	        			
	        		}
	        		
	        		else
	        		{
		        		command = Integer.parseInt(cc[0]);
		        		if (command == Controls.QUIT_SERVER)
			        	{	
			        		System.out.println("finish server");
			        		try{
			        			if(frame!=null)
			        				frame.dispose();
			        			System.exit(0);
			        		}
			        		catch(Exception e)
			        		{
			        			e.printStackTrace();
			        			System.exit(1);
			        		}
		        			break;
			        	}
			        	else if (command == Controls.QUIT_APP)
			        	{	
			        		System.out.println("finish app");
			        	
			        		try
			        		{
			        			if(frame!=null)
			        				frame.dispose();
			        		}
			        		catch(Exception e)
			        		{
			        			e.printStackTrace();
			        		}
			        		break;
			        	}
			        	else if(command==Controls.NEXT)
		        		{
		        			if(frame!=null && frame.getState()==Frame.NORMAL)
		        			{
			        			frame.setState(Frame.ICONIFIED);
			        			Thread.sleep(sleepTime);
		        			}
		        			
		        			Robot r=new Robot();
		        			r.keyPress(KeyEvent.VK_DOWN);
		        			r.keyRelease(KeyEvent.VK_DOWN);
		        			outputStream.write(Controls.SUCCESS);
		        		}
		        		
		        		else if(command==Controls.PREVIOUS)
		        		{
		        			if(frame!=null && frame.getState()==Frame.NORMAL)
		        			{
			        			frame.setState(Frame.ICONIFIED);
		        				Thread.sleep(sleepTime);
		        			}
		        			Robot r=new Robot();
		        			r.keyPress(KeyEvent.VK_UP);
		        			r.keyRelease(KeyEvent.VK_UP);
		        			outputStream.write(Controls.SUCCESS);
		        			
		        		}
		        	    else if(command==Controls.DRAW)
		        		{
		        			
		        			if(frame!=null && frame.getState()==Frame.NORMAL)
		        			{
		        				System.out.println("normal to iconfified");
		        				frame.setState(Frame.ICONIFIED);
		        				Thread.sleep(sleepTime);
			        			
		        			}
		        			else if(frame!=null && frame.getState()==Frame.ICONIFIED) 
		        			{
		        				System.out.println("ic to no");
		        				launchPanel(new DrawPanel(image));
		        			}
		        			else
		        			{
		        				System.out.println("new");
		        				launchPanel(new DrawPanel());
		        			}
		        			outputStream.write(Controls.SUCCESS);
		        			
		        			
		        		}
		        		
	        		}
	        	}
	        	catch(Exception e)
	        	{
	        		e.printStackTrace();
	        		System.out.println("error");
	        	}
	        }
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(frame!=null)
				frame.dispose();
		}
	}
	
	private boolean launchPanel(DrawPanel dp) 
	{
		try{
			System.out.println(ht+"   "+width);
		
			if(frame!=null)
			{
				frame.dispose();
				frame=null;
			}
		    frame = new JFrame();
		    frame.add(dp);
		    frame.setUndecorated(true);
		    frame.setBackground(new Color(0,0,0,0));
		    frame.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    if (e.getClickCount() == 2) {
                    	frame.dispose();
                    	frame=null;
                    }
                }
                });
		    frame.setVisible(true);
		    Dimension dim = Toolkit.getDefaultToolkit().getScreenSize(); 
		    scaleWidth=dim.width-70;
		    scaleHeight=dim.height-30;
		    frame.setSize(scaleWidth,scaleHeight);
		    frame.setLocation(5,5);
		    
		    return true;
		}
		catch(Exception e)
		{
			return false;
		}
	}
	
	
}
		